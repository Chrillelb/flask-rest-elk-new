# Using flask to make an api
# import necessary libraries and functions

from importlib.resources import Resource, path
from flask import Flask,request,jsonify


import tensorflow as tf
from tensorflow import keras
import numpy as np
import io 
import pickle as p
from PIL import Image
from pathlib import Path

model = tf.keras.models.load_model(r'model_dir');
# model.save("model_dir");


# prepare image functionality 
def prepare_image(img):
	img = Image.open(io.BytesIO(img))
	img = img.resize((224,224))
	img = np.array(img)
	img = np.expand_dims(img,0)
	return img

def predict_result(img): 
	return "EMPTY"  if model.predict(img)[0][0] > 0.5 else "Animal"


# creating a Flask app
app = Flask(__name__)

# on the terminal type: curl http://127.0.0.1:5000/
# returns hello world when we use GET.
# returns the data that we send when we use POST.
@app.route('/predict', methods = ['POST'])
def infer_image(): 
	if 'file' not in request.files: 
		return "Please try again. The image doesn't exist"
	file = request.files.get('file')
	if not file: 
		return

	img_bytes = file.read()

	img = prepare_image(img_bytes)
	return jsonify(prediction =str(model.predict(img)[0][0]))

@app.route('/', methods = ['GET'])
def home():

		def index(): 
			return 'Machine Learning Inference'


	# driver function
if __name__ == '__main__':
	app.run(debug = True)


    